
Example 2: An interactive plot - parameter vs. time
===================================================

Author: Michael Betz, CERN BE-BI

Setup
-----

.. code:: python

    %matplotlib qt
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from IPython.display import display
    import numpy as np

For this example we will initialize PyJapc in "safe mode" so all sets
are only simulated (i.e. are not written to the device).

.. code:: python

    import pyjapc
    
    japc = pyjapc.PyJapc( noSet=True )
    
    japc.setSelector("SCT.USER.ALL")

Create the initial plot
-----------------------

The plot will be updated with new data in the next step.

We will use a list() to buffer the values for plotting. Compared to
numpy arrays, lists are slow but easily extendable.

.. code:: python

    valueBuffer = list()

This will create an empty plot window with no data yet. Please keep it
open for the next steps.

.. code:: python

    line, = plt.plot( 0, "-o" )
    plt.axis((0, 10, -1, 1))
    plt.xlabel("Sample no.")
    plt.ylabel("Current [{0}]".format( japc.getParam("CB.BHB1100/Acquisition#current_unit") ) )
    plt.draw()

Define a simple callback function, which will update the plot when new data arrives
-----------------------------------------------------------------------------------

.. code:: python

    def myValueCallback( parameterName, newValue ):
        global valueBuffer, line
        valueBuffer.append( newValue ) # Just append the new data to the buffer
        line.set_data( range(len(valueBuffer)) ,valueBuffer )
        line.figure.canvas.draw()

Start the subscription
----------------------

.. code:: python

    valueBuffer.clear()
    japc.subscribeParam( "CB.BHB1100/Acquisition#currentAverage", myValueCallback )
    japc.startSubscriptions()

.. code:: python

    display(plt.gcf())



.. image:: example_2_files/example_2_13_0.png


.. code:: python

    japc.stopSubscriptions()

Alternatively: A more fancy callback could implement a rolling view like this
-----------------------------------------------------------------------------

.. code:: python

    def myValueCallback( parameterName, newValue ):
        global valueBuffer, line
        valueBuffer.append( newValue ) # append the new data to the buffer
        if len( valueBuffer ) > 10:    # Keep max. 10 points in the buffer
            valueBuffer.pop(0)         # Remove the oldest point
        line.set_data( range(len(valueBuffer)) ,valueBuffer )
        line.figure.canvas.draw()

.. code:: python

    japc.startSubscriptions()

.. code:: python

    japc.stopSubscriptions()
